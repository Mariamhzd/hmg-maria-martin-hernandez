<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends AbstractController
{
    /**
     * @Route("/", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/new-user")
     */

    public function register(UserPasswordEncoderInterface $encoder, EntityManagerInterface $doctrine )
    {
        $usuario = new User();
        $usuario->setUsername('Maria');
        $usuario->setPassword($encoder->encodePassword($usuario, '123456'));
        $usuario->setEmail('maria@maria.com');
        $usuario->setCommentary('Hola, soy un comentario');

        $doctrine->persist($usuario);
        $doctrine->flush();

        return $this->render('users/userlist.html.twig');

    }

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    //Ruta para mostrar listado de usuarios:

    /**
     * @Route("/users", name="users")
     * 
     */
    public function userList(EntityManagerInterface $doctrine)
    {
        $userlist = $doctrine->getRepository(User::class);

        $user = $userlist->findAll();

        return $this->render("users/userList.html.twig",
        ['user' => $user]);
    }

    //Ruta para crear un nuevo usuario:

    /**
     * @Route("/users/create-user", name="create_user")
     */
    public function createUser(Request $request, EntityManagerInterface $doctrine)
    {        
        $user = new User();
        $form = $this->createForm(UserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){            
            $user = $form->getData();

            //codificación password
            $pass = $user->getPassword();
            $encoded = $this->passwordEncoder->encodePassword($user, $pass);
            $user->setPassword($encoded);

            $doctrine->persist($user);
            $doctrine->flush();       

            return $this->redirectToRoute('users');
        }                 
   
        return $this->render("users/createUser.html.twig",[
            'usersForm' => $form->createView(),
        ]);
    }

    //Ruta para editar usuario:

    /**
     * @Route("/users/edit/{id}", name="user_edit")
     */
    public function editUser(User $user,Request $request, EntityManagerInterface $doctrine)
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $user = $form->getData();
            $doctrine->persist($user);
            $doctrine->flush();
            
            return $this->redirectToRoute('users');

        }            
            return $this->render('users/editUser.html.twig', [
                    'editForm' => $form->createView(),
            ]);
    }

    //Ruta para borrar usuario:

    /**
     * @Route("/users/delete/{id}", name="user_delete")
     */
    public function deleteUser(Request $request, User $user)
    { 
        if ($user === null) {
            return $this->redirectToRoute('users');
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($user);
        $doctrine->flush();

        return $this->redirectToRoute('users');
    }

}


